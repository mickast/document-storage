var sortCheck = false;
var svQuery = '';
function sort(name){
    var sortVal = true;
    if(sortCheck == true){
        sortVal = 'ASC';
        sortCheck = false;
    } else {
        sortVal = 'DESC';
        sortCheck = true;
    }
    $.ajax({
        url: Routing.generate('sort'),
        type: "POST",
        dataType: "html",
        jsonpCallback: 'callback',
        data: {sort: sortVal, field: name, svquery: svQuery},
        contentType: "application/x-www-form-urlencoded",
        success: function (data) {
            $('tbody').html('').append(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("You can not send Cross Domain AJAX requests: " + errorThrown);
        }
    });

}

function search(query){
    svQuery = query;
    $.ajax({
        url: Routing.generate('search'),
        type: "POST",
        dataType: "html",
        jsonpCallback: 'callback',
        data: {query: query},
        contentType: "application/x-www-form-urlencoded",
        success: function (data) {
            $('.searchResults').css('display', 'block');
            $('.res').html(query);
            $('tbody').html('').append(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("You can not send Cross Domain AJAX requests: " + errorThrown);
        }
    });
};


   $('#myform').submit( function(e) {
        e.preventDefault();
    }).validate({
            rules: {
                userName: {
                    required: true,
                    minlength: 3,
                    maxlength: 10
                },
    
                login: {
                    required: true,
                    minlength: 2,
                    maxlength: 10
                },
                pass: {
                    required: true,
                    minlength: 5,
                    maxlength: 20
                },
                repass: {
                    required: true,
                    minlength: 5,
                    maxlength: 20
                }
        },
   
    messages: {
    userName: {
         required: "Введите Ваше имя!",
         minlength: "Ваше имя должно содержать минимум 3 символов!",
         maxlength: "Это поле может содержать максимум 10 символов"
    },
      login: {
        required: "Введите логин!",
        minlength: "Логин должен содержать минимум 2 символов!",
        maxlength: "Это поле может содержать максимум 10 символов"
      },
      pass: {
        required: "Введите пароль!",
        minlength: "Ваш пароль должен быть минимум 5 символов!",
        maxlength: "Это поле может содержать максимум 20 символов"
      },
      repass: {
        required: "Введите повторный пароль!!",
        minlength: "Ваш пароль должен быть минимум 5 символов!",
        maxlength: "Это поле может содержать максимум 20 символов"
      }
    },
  
    submitHandler: function(form) {
      form.submit();
    }
  });
  
  $('.log').submit( function(e) {
        // e.preventDefault();

    }).validate({
            rules: {
                _username: {
                    required: true
                },
    
                _password: {
                    required: true
                }
        },
   
    messages: {
   
    _username: {
         required: "Введите логин!"
    },
      _password: {
        required: "Введите пароль!"
      }
    },
  
    submitHandler: function(form) {
      form.submit();
    }
  });



 $('.addDocum__form').submit( function(e) {

    }).validate({
            rules: {
                'document[title]': {
                    required: true,
                    minlength: 3
                },
    
                'document[file]': {
                    required: true,
                },
        },
   
    messages: {
   
    'document[title]': {
         required: "Введите название файла!",
         minlength: "Название файла должно содержать минимум 3 символа!"
    },
      'document[file]': {
        required: "Выберите файл!"
      }
    },
  
    submitHandler: function(form) {
      form.submit();
    }
  });