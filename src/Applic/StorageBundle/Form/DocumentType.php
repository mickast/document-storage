<?php

namespace Applic\StorageBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
class DocumentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array('label' => 'Название')
                 )
//            ->add('date', 'date')
//            ->add('author', 'text')
            ->add('file', 'file', array('label' => 'Файл'))
            ->add('Добавить','submit', 
            array(
                 'attr' => array('class' => 'button button--winona button--border-thin button--text-thick button--inverted')
                 ));
    }
    public function getName()
    {
        return 'document';
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Applic\StorageBundle\Entity\Document'
        ));
    }
}