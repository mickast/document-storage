<?php


namespace Applic\StorageBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File;
use Applic\StorageBundle\Entity\Document;
use Applic\StorageBundle\Form\DocumentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DocumentController extends Controller
{
    private $entCriteriaList = [];

    public function indexAction(Request $request){
        $author = $this->getUser()->getFio();
        $this->entCriteriaList['author'] = $author;
        $em = $this->getDoctrine()->getManager();
        $docs = $em->getRepository('ApplicStorageBundle:Document')->findBy(array('author' => $author));

        $doc = new Document();
        $form = $this->createForm(new DocumentType(), $doc);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid() && $request->getMethod() == 'POST') {
            $file = $form['file']->getData();
            $fileTitle = $form['title']->getData();
            $fileExt = $file->guessClientExtension();
            if(!$fileExt){
                $fileExt = 'bin';
            }
            $endFile = $fileTitle . '.' .$fileExt;

            $file->move($this->get('kernel')->getRootDir(). '/Resources/files/', $endFile);
            $nowDate = new \DateTime();

            $docu = new Document();
            $docu->setTitle($fileTitle);
            $docu->setAuthor($author);
            $docu->setDate($nowDate);
            $docu->setFile($file);
            $docu->setExt($fileExt);
            $docu->setPath($endFile);
            $em = $this->getDoctrine()->getManager();
            $em->persist($docu);
            $em->flush();
            return $this->redirect($this->generateUrl('admin_home'));
        }

        return $this->render('ApplicStorageBundle:Document:index.html.twig', array('form' => $form->createView(), 'docs' => $docs));
    }

    public function downloadFileAction($filename){

        $path = $this->get('kernel')->getRootDir(). '/Resources/files/' . $filename;
        $content = file_get_contents($path);

        $response = new Response();

        $response->headers->set('Content-Type', 'text');
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);

        $response->setContent($content);

        return $response;
    }
    
    public function sortAction(Request $request){
        $sort = $request->get('sort');
        $svquery = $request->get('svquery');
        $field = $request->get('field');
        $author = $this->getUser()->getFio();
        $this->entCriteriaList['author'] = $author;
        $sortArr = [];
        $sortArr[$field] = $sort;
        $em = $this->getDoctrine()->getManager();
        $docs = $em->getRepository('ApplicStorageBundle:Document')->findBy($this->entCriteriaList, $sortArr);
        
        $newdocs = [];
        if($svquery != ''){
          for($i = 0; $i < count($docs); $i++){
            if(strstr($docs[$i]->getTitle(), $svquery)){
                $newdocs[] = $docs[$i];
            }
        }  
        } else {
            $newdocs = $docs;
        }
        
        $doc = new Document();
        $form = $this->createForm(new DocumentType(), $doc);


        $templating = $this->container->get('templating');
        return $templating->renderResponse(
            'ApplicStorageBundle:Document:table.html.twig',
            array('form' => $form->createView(),'docs' => $newdocs)
        );

    }

    public function searchAction(Request $request){
        $author = $this->getUser()->getFio();
        $query = $request->get('query');
        $this->entCriteriaList['title'] = $query;
        $em = $this->getDoctrine()->getManager();
        $docs = $em->getRepository('ApplicStorageBundle:Document')->findBy(array('author' => $author));

        $newdocs = [];
        for($i = 0; $i < count($docs); $i++){
            if(strstr($docs[$i]->getTitle(), $query)){
                $newdocs[] = $docs[$i];
            }
        }

        $doc = new Document();
        $form = $this->createForm(new DocumentType(), $doc);


        $templating = $this->container->get('templating');
        return $templating->renderResponse(
            'ApplicStorageBundle:Document:table.html.twig',
            array('form' => $form->createView(),'docs' => $newdocs, 'query' => $query)
        );
    }
}