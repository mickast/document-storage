<?php

namespace Applic\StorageBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Applic\StorageBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class SecurityController extends Controller
{
    public function loginAction(){

        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('ApplicStorageBundle:Security:login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }


    public function regAction(Request $request){

        if($request->getMethod() == 'POST'){

            $userName = $request->get('userName');
            $login = $request->get('login');
            $pass = $request->get('pass');
            $repass = $request->get('repass');
            if($pass == $repass){

                $manager = $this->getDoctrine()->getEntityManager();


                $user = new User();
                $user->setFio($userName);
                $user->setUsername($login);
                $user->setSalt(md5(time()));

                $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
                $password = $encoder->encodePassword($pass, $user->getSalt());
                $user->setPassword($password);



                $manager->persist($user);

                $manager->flush();


                $mess = "Все хорошо";
            } else {
                $mess = 'Пароли не совпадают';
            }

            return $this->render('ApplicStorageBundle:Registration:index.html.twig', array(
                "mess" => $mess,
            ));
        }
        return $this->render('ApplicStorageBundle:Registration:index.html.twig');
    }
}