var sortCheck = false;
function sort(name){
    var sortVal = true;
    if(sortCheck == true){
        //sortVal = sortCheck;
        sortVal = 'ASC';
        sortCheck = false;
    } else {
        //sortVal = sortCheck;
        sortVal = 'DESC';
        sortCheck = true;
    }

    $.ajax({
        url: Routing.generate('sort'),
        type: "POST",
        dataType: "html",
        jsonpCallback: 'callback',
        data: {sort: sortVal, field: name},
        contentType: "application/x-www-form-urlencoded",
        //contentType: 'application/json; charset=utf-8; ',
        success: function (data) {
            //console.log(JSON.stringify(data));
            console.log(data);
            $('tbody').html('').append(data);
            //$('span').html('sdf');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("You can not send Cross Domain AJAX requests: " + errorThrown);
        }
    });
    //$.ajax({
    //    url: 'documents/sort',
    //    type: "POST",
    //    dataType: "json",
    //    contentType: "application/x-www-form-urlencoded; charset=utf-8",
    //    success: function (data) {
    //        workItemTypes = JSON.parse(data);
    //    },
    //    error: function (jqXHR, textStatus, errorThrown) {
    //        alert("You can not send Cross Domain AJAX requests: " + errorThrown);
    //    }
    //});
}
function search(query){
    $.ajax({
        url: Routing.generate('search'),
        type: "POST",
        dataType: "html",
        jsonpCallback: 'callback',
        data: {query: query},
        contentType: "application/x-www-form-urlencoded",
        //contentType: 'application/json; charset=utf-8; ',
        success: function (data) {
            //console.log(JSON.stringify(data));
            $('.searchResults').css('display', 'block');
            $('.res').html(query);
            console.log(data);
            $('tbody').html('').append(data);
            //$('span').html('sdf');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("You can not send Cross Domain AJAX requests: " + errorThrown);
        }
    });
}